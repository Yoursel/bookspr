﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Books
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void OnDelete(object sender, RoutedEventArgs e)
        {
            var dataContext = ((Button)sender).DataContext;
            var book = (Book)dataContext;
            ((ViewModel)DataContext).DeleteBook(book);
        }

        private void OnEdit(object sender, RoutedEventArgs e)
        {
            var bookEdit = new BookEdit();
            bookEdit.ShowDialog();
        }
    }
}
