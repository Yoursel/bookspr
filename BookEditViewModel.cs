﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Books
{
    public class BookEditViewModel : INotifyPropertyChanged
    {
        private string imgEdit;

        public event PropertyChangedEventHandler PropertyChanged;

        public string NameEdit { get; set; }
        public DateTime? DateEdit { get; set; }
        public string ImgEdit 
        { 
            get => imgEdit;
            set
            {
                imgEdit = value;

                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(ImgEdit)));
            }
        }
        public List<Author> Authors { get; set; }
    }

}
