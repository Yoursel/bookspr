using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Books
{
    public class ViewModel
    {
        public ViewModel()
        {
            
            Books.Add(new Book()
            {
                Id = 2,
                Name = "Война и мир",
                Date = new DateTime(1956, 01, 01),
                Author = Authors.Skip(1).First(),
                Image = "https://knigotvorec.ru/image/cache/catalog/i/in/id/ecb0057ba0c78aa986d787339b52cc8e-800x800.jpg"
            });
            
            Books.Add(new Book()
            {
                Id = 3,
                Name = "Сказка о царе Салтане",
                Date = new DateTime(1986, 01, 01),
                Author = Authors.First(),
                Image = "https://umitoy.ru/upload/iblock/ea8/ea85cc8355ba794548ec66fb1eb9702e.jpg"
            });

             Books.Add(new Book()
            {
                Id = 4,
                Name = "C# (5е изд.)",
                Date = new DateTime(1986, 01, 01),
                Author = Authors.First(),
                Image = "https://balka-book.com/files/2021/09_15/14_19/u_files_store_25_28535.jpg"
            });

            #region DB

            /*
            var connString = ConfigurationManager.ConnectionStrings["books"].ConnectionString;

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmdRead = new SqlCommand("select * from books" , conn);
            */

            /*
            SqlParameter p1 = new SqlParameter("@p1", firstNameFirstLetter);
            SqlParameter p2 = new SqlParameter("@p2", firstNameLastLetter);
            cmdRead.Parameters.Add(p1);
            cmdRead.Parameters.Add(p2);
            */
            /*
            try
            {
                conn.Open();
                SqlDataReader rdr = cmdRead.ExecuteReader();
                while (rdr.Read())
                {
                    Console.WriteLine(rdr["FirstName"] + " " + rdr["LastName"]);
                }
                rdr.Close();
            }
            finally
            {
                conn.Close();
            }
            */

            #endregion
        }

        public void DeleteBook(ulong bookId)
        {
            var book = Books.FirstOrDefault(x => x.Id == bookId);
            if (book == null) return;

            Books.Remove(book);
        }

        public void DeleteBook(Book book)
        {
            if (book == null) return;
            Books.Remove(book);
        }

        public ObservableCollection<Book> Books { get; set; } = new ObservableCollection<Book>();

        public ObservableCollection<Author> Authors { get; set; } = new ObservableCollection<Author>
        {
            new Author()
            {
                Id = 10,
                Name = "Александр",
                LastName = "Сергеевич",
                SurName = "Пушкин"
            },
            new Author()
            {
                Id = 20,
                Name = "Лев",
                LastName = "Николаевич",
                SurName = "Толстой"
            }
        };
    }


    public class Author
    {
        public ulong Id { get; set; }
        public string Name { get; set; }
        public string SurName { get; set; }
        public string LastName { get; set; }
        public string AuthorFormated => $"{Name} {LastName} {SurName}";
    }

    public class Book
    {
        public ulong Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public DateTime Date { get; set; }
        public Author Author { get; set; }
        public string DateFormated => Date.ToString("dd.MM.yyyy");        
    }
}
